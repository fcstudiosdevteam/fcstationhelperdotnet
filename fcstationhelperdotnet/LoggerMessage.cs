﻿namespace FCStationHelperDotNet
{
    /// <summary>
    /// A class that defines a log message
    /// </summary>
    public struct LoggerMessage
    {
        #region Public Properties
        /// <summary>
        /// The date of this message
        /// </summary>
        public  string Date { get; set; }

        /// <summary>
        /// The time of this message
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// The message of this log entry
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Boolean if log is created from an object export
        /// </summary>
        public bool IsObject { get; set; }

        /// <summary>
        /// Location of the model if the log is created from an object export
        /// </summary>
        public string ModelLocation { get; set; }

        /// <summary>
        /// The type of log
        /// </summary>
        public LogType LogType { get; set; }
        #endregion
    }
}