﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;

namespace FCStationHelperDotNet
{
    public static class LogSystem
    {
        private static readonly HashSet<LoggerMessage> LoggerMessages = new HashSet<LoggerMessage>();

        #region Public Properties
        /// <summary>
        /// The current directory for the FCSStation.
        /// </summary>
        public static string FCStationDirectory { get; set; }
        #endregion

        public static void Initialize()
        {
            // Set FCStationDirectory
            var processModule = Process.GetCurrentProcess().MainModule;

            if (processModule != null)
            {
                FCStationDirectory = FCStationDirectory = Path.GetDirectoryName(processModule.FileName);
            }
            else
            {
                FCStationDirectory = AppDomain.CurrentDomain.BaseDirectory;
            }
        }


        #region Public Methods

        /// <summary>
        /// Gets the current date and time
        /// </summary>
        /// <returns></returns>
        public static string CurrentDateTime()
        {
            return DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// A method to add a message to the messages list
        /// </summary>
        /// <param name="message">The message to show</param>
        /// <param name="loggedDateTime">The date and time the log was entered</param>
        /// <param name="logType">Switches the icon if it is an error message default is None</param>
        /// <param name="isObject">Bool that states if the log has been created from a model export</param>
        /// <param name="modelLocation">Location of the exported object</param>
        public static LoggerMessage Message(string message, string loggedDateTime, LogType logType = LogType.Info, bool isObject = false, string modelLocation = "")
        {
            // Convert the logged date and time to a DateTime
            var dateTime = Convert.ToDateTime(loggedDateTime, CultureInfo.InvariantCulture);

            #region Backups
            //date = DateTime.Date == DateTime.Today ? "Today" : DateTime.ToLongDateString();
            //time = DateTime.Date == DateTime.Today ? DateTimeConverter.TimeAgo(DateTime) : DateTime.ToLongTimeString(); 
            #endregion

            // The current date
            var date = dateTime.ToLongDateString();
            // The current time
            var time = dateTime.ToLongTimeString();

            var loggerMessage = new LoggerMessage();

            // Create a message
            loggerMessage.Date = date;
            loggerMessage.Time = time;
            loggerMessage.Message = message;
            loggerMessage.IsObject = isObject;
            loggerMessage.ModelLocation = modelLocation;
            loggerMessage.LogType = logType;

            return loggerMessage;
        }

        /// <summary>
        /// A method to add a message to the messages list and gets current date and time then saved
        /// </summary>
        /// <param name="message">The message to show</param>
        /// <param name="logType">Switches the icon if it is an error message default is None</param>
        /// <param name="isObject">Bool that states if the log has been created from a model export</param>
        /// <param name="modelLocation">Location of the exported object</param>
        public static void Message(string message, LogType logType = LogType.Info, bool isObject = false, string modelLocation = "")
        {
            // Create Message
            var loggedMessage = Message(message, CurrentDateTime(), logType, isObject, modelLocation);

            Console.WriteLine(message);

            //Save log
            AppendToLog(loggedMessage);
        }

        private static void AppendToLog(LoggerMessage loggedMessage)
        {
            LoggerMessages.Add(loggedMessage);
        }

        public static void ErrorMessage(Exception e, string message = "")
        {
            Message($"{message} \n {e.Message} \n {e.StackTrace}");
        }

        public static void ErrorMessage(string message = "")
        {
            Message(message);
        }


        #endregion

        /// <summary>
        /// Saves the log to the log xml
        /// </summary>
        public static void SaveLog()
        {
            try
            {
                //Log directory Location
                var fcsStationLogDirectory = Path.Combine(FCStationDirectory, "System", "Log");

                //Check if Log directory exist if not create it.
                if (!FileOperations.DirectoryExist(fcsStationLogDirectory))
                {
                    FileOperations.CreateDirectory(fcsStationLogDirectory);
                }

                //XFile save location
                var logXmlSaveLocation = Path.Combine(fcsStationLogDirectory, "FCSStation_logs.json");

                
                if (FileOperations.FileExist(logXmlSaveLocation))
                {
                    FileOperations.DeleteFile(logXmlSaveLocation);
                }

                // serialize JSON directly to a file
                using (var file = File.CreateText(logXmlSaveLocation))
                {
                    var serializer = new JsonSerializer {Formatting = Formatting.Indented};
                    serializer.Serialize(file, LoggerMessages);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}\n{e.StackTrace}");
            }
        }
    }
}
