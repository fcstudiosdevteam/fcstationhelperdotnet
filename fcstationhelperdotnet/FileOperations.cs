﻿using System;
using System.IO;
using System.Linq;

namespace FCStationHelperDotNet
{
    internal static class FileOperations
    {
        /// <summary>
        /// Safely check if a directory exist.
        /// </summary>
        /// <param name="path">The path to the directory to check for.</param>
        /// <returns></returns>
        internal static bool DirectoryExist(string path)
        {
            if (Directory.Exists(path))
            {
                LogSystem.Message($"Directory {path} does exist!");
                return true;
            }
            else
            {
                LogSystem.Message($"Directory {path} doesn't exist!", LogType.Error);
                return false;
            }

        }

        /// <summary>
        /// Creates a directory if it doesn't exist
        /// </summary>
        /// <param name="directoryPath"></param>
        internal static void CreateDirectory(string directoryPath)
        {
            try
            {
                LogSystem.Message(string.Format("Attempting to create directory: {0}", directoryPath));

                if (!DirectoryExist(directoryPath))
                {
                    LogSystem.Message(string.Format("Directory {0} doesnt exist creating it now.", directoryPath));
                    Directory.CreateDirectory(directoryPath);
                    LogSystem.Message(string.Format("{0} has been created.", directoryPath));
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }
        }

        internal static void DeleteFile(string convertXfileLocation)
        {
            try
            {
                if (FileExist(convertXfileLocation))
                {
                    File.Delete(convertXfileLocation);
                }
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e);
            }
        }

        /// <summary>
        /// Safely check if a file exist.
        /// </summary>
        /// <param name="filePath">The path to the file to check for.</param>
        /// <returns></returns>
        internal static bool FileExist(string filePath)
        {
            if (File.Exists(filePath))
            {
                LogSystem.Message($"File {filePath} does exist!");
                return true;
            }
            else
            {
                LogSystem.Message($"File {filePath} doesn't exist!", LogType.Error);
                return false;
            }

        }
    }
}
