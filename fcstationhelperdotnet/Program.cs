﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ASToFra.ObjectModel3D;
using Newtonsoft.Json;

namespace FCStationHelperDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            LogSystem.Initialize();

            //args  = new string[] {@"E:\Flight Simulation Tools\ModelConverter X", @"F:\System Folders\Desktop\Test", "--MCXToJSON" };

            LogSystem.Message($"Arguments Count: {args.Length}");

            if (args.Length > 0)
            {
                if (!args[2].Contains("--MCXToJSON")) return;
                LogSystem.Message("Converting Templates To JSON");
                ExportMCXMaterialToJson(args[0], args[1]);
            }
            else
            {
                LogSystem.Message("This application must be started by a command from FCStation please do not try to use this tool separately");
            }

            LogSystem.SaveLog();
        }

        static void ExportMCXMaterialToJson(string MCXDirectory, string exportLocation)
        {
            try
            {
                LogSystem.Message("Beginning MCXTemplate to JSON Conversion.");
                //Check if the directory exist
                if (!FileOperations.DirectoryExist(MCXDirectory))
                {
                    throw new DirectoryNotFoundException();
                }

                if (!FileOperations.DirectoryExist(exportLocation))
                {
                    throw new DirectoryNotFoundException();
                }

                var templateDirectory = Path.Combine(MCXDirectory, "templates");

                LogSystem.Message($"Getting the materials located in the target directory: {templateDirectory}");

                foreach (string mcxMaterialPath in GetMCXMaterials(templateDirectory))
                {
                    var fileName = Path.GetFileName(mcxMaterialPath);
                    LogSystem.Message($"Converting: {fileName} to : {Path.GetFileNameWithoutExtension(mcxMaterialPath)}.json");
                    
                    MaterialTemplateUser materialTemplate = MaterialTemplateUser.Read(mcxMaterialPath);

                    LogSystem.Message($"Path: {mcxMaterialPath} || Template: {materialTemplate}");

                    if (materialTemplate == null)
                    {
                        LogSystem.ErrorMessage($"Material Template returned null for: {mcxMaterialPath}");
                        continue;
                    }

                    // serialize JSON to a string and then write string to a file
                    File.WriteAllText(Path.Combine(exportLocation, $"{materialTemplate.Name}.json"),
                        JsonConvert.SerializeObject(materialTemplate, Formatting.Indented));

                    LogSystem.Message($"{fileName} Conversion successful.");
                }

                LogSystem.Message($"Converting Complete.");
            }
            catch (Exception e)
            {
                LogSystem.ErrorMessage(e, "Failed to convert MCX Material to JSON");
            }
        }

        private static IEnumerable<string> GetMCXMaterials(string mcxMaterialDirectory)
        {
            return Directory.GetFiles(mcxMaterialDirectory, "*.mcxmat");
        }
    }
}
